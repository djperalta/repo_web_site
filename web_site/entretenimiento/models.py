from django.db import models

# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=50, verbose_name = 'nombre')
    publicado = models.BooleanField(default = True)
    fecha_registro = models.DateTimeField(auto_now_add = True)
    fecha_modificacion = models.DateTimeField(auto_now = True)
    
    def __unicode__(self):
        return self.nombre

class SubCategoria(models.Model):
    categoria_id = models.ForeignKey(Categoria)
    nombre = models.CharField(max_length=50, verbose_name = 'nombre')
    publicado = models.BooleanField(default = True)
    fecha_registro = models.DateTimeField(auto_now_add = True)
    fecha_modificacion = models.DateTimeField(auto_now = True)
    
    def __unicode__(self):
        return self.nombre
