from django.contrib import admin
#importamos nuestro modelo
from entretenimiento.models import Categoria, SubCategoria

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ("nombre","publicado") #campos que seran mostrados
admin.site.register(Categoria, CategoriaAdmin)

class SubCategoriaAdmin(admin.ModelAdmin):
    list_display = ("nombre","publicado")
admin.site.register(SubCategoria, SubCategoriaAdmin)
